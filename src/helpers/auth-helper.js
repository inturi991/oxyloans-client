import Axios from "./axios-config";

export const userLogin = (data) => {
  return Axios.post(`api/auth/sign-in`, data)
    .then((response) => {
      if (response.status === 400 || response.status === 500)
        throw response.data;
      return response.data;
    })
    .catch((err) => {
      throw Error("Incorrent username or password");
    });
};