import React, { useState } from "react"
import { useHistory } from "react-router-dom"
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Input,
  Label,
  Button,
  Alert,
} from "reactstrap"
import "react-bootstrap-typeahead/css/Typeahead.css"
import { Form, Formik, ErrorMessage } from "formik"
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

import * as yup from "yup"

import { FieldError } from "../../components/Common/FieldError"
import { createCompany } from "../../services/companies"

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const schema = yup.object().shape({
  name: yup.string().required("Company Name is required"),
  email: yup.string().required("Email is required"),
  // phoneNumber: yup.string().required("Phone Number is required"),
  mobileNumber: yup
    .string()
    .required("Phone Number is required")
    .matches(phoneRegExp, "Only Numbers Allowed")
    .min(10, "Minimum 10 Digits Allowed")
    .max(10, "Maximum 10 Digits Allowed"),

  address: yup.string().required("Address is required"),
})

const CompanyCreate = () => {
  const { push } = useHistory()
  const [errorMessage, setErrorMessage] = useState("")

  const initialValues = {
    name: "",
    email: "",
    alternativeEmail:"",
    mobileNumber: "",
    alternativeMobileNumber: "",
    address: "",
    description: "",
  }

  const handleCreateCompany = formValues => {
    createCompany(formValues)
      .then(() => push("/companies"))
      .catch(err => {
        if (err.response) {
          const {
            data: { message },
          } = err.response
          setErrorMessage(message)
        } else {
          setErrorMessage(err.message)
        }
      })
  }

  return (
    <>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Companies" breadcrumbItem="Create Company" />

          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  {errorMessage && <Alert color="danger">{errorMessage}</Alert>}

                  <Formik
                    initialValues={initialValues}
                    validationSchema={schema}
                    onSubmit={handleCreateCompany}
                  >
                    {({
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      values,
                      errors,
                      touched,
                      setFieldValue,
                    }) => {
                      return (
                        <div>
                          <Form onSubmit={handleSubmit}>
                            <Row>
                              <Col sm="6">
                                <div className="mb-3">
                                  <Label htmlFor="name">Company Name</Label>
                                  <Input
                                    tabIndex="1"
                                    id="name"
                                    name="name"
                                    type="text"
                                    className={
                                      "form-control" +
                                      (errors.name && touched.name
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Company Name"
                                  />

                                  <ErrorMessage
                                    name="name"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="email">Email</Label>
                                  <Input
                                    tabIndex="3"
                                    id="email"
                                    name="email"
                                    type="text"
                                    onChange={handleChange}
                                    className={
                                      "form-control" +
                                      (errors.email && touched.email
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Email ID"
                                  />
                                  <ErrorMessage
                                    name="email"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="alternativeEmail">Alternative Email</Label>
                                  <Input
                                    tabIndex="5"
                                    id="alternativeEmail"
                                    name="alternativeEmail"
                                    type="text"
                                  
                                    onChange={handleChange}
                                    placeholder="Enter Your Alternative Email"
                                  />
                            
                                </div>

                                <div className="mb-3">
                                  <Label htmlFor="address">Address</Label>
                                  <Input
                                    tabIndex="7"
                                    id="address"
                                    name="address"
                                    type="textarea"
                                    className={
                                      "form-control" +
                                      (errors.address && touched.address
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Address"
                                  />
                                  <ErrorMessage
                                    name="address"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                              </Col>
                              <Col sm="6">
                                <div className="mb-3">
                                  <Label htmlFor="mobileNumber">
                                    Mobile Number
                                  </Label>
                                  <Input
                                    tabIndex="2"
                                    id="mobileNumber"
                                    name="mobileNumber"
                                    type="text"
                                    className={
                                      "form-control" +
                                      (errors.mobileNumber && touched.mobileNumber
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Phone Number"
                                  />
                                  <ErrorMessage
                                    name="mobileNumber"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="headQuarters">
                                  Alternative Mobile Number
                                  </Label>
                                  <Input
                                    tabIndex="4"
                                    id="alternativeMobileNumber"
                                    name="alternativeMobileNumber"
                                    type="text"
                                    onChange={handleChange}
                                    placeholder="Enter Your Alternative Mobile Number"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="description">
                                    Description
                                  </Label>
                                  <Input
                                    tabIndex="6"
                                    id="description"
                                    name="description"
                                    type="textarea"
                                    onChange={handleChange}
                                  />
                                </div>
                              </Col>
                            </Row>
                            <div className="d-flex flex-wrap gap-2">
                              <Button
                                type="submit"
                                color="primary"
                                className="btn "
                              >
                                Save
                              </Button>
                              <Button
                                type="button"
                                color="secondary"
                                className=" "
                                onClick={() => push("/companies")}
                              >
                                Cancel
                              </Button>
                            </div>
                          </Form>
                        </div>
                      )
                    }}
                  </Formik>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

export default CompanyCreate
