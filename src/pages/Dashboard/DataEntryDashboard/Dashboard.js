import React from "react"
import MetaTags from "react-meta-tags"
import { Container, Row, Col } from "reactstrap"

// Pages Components
import WelcomeComp from "../WelcomeComp"

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

//i18n
import { withTranslation } from "react-i18next"

const Dashboard = props => {
  console.log(props, "DASHBOARD DATA ENTRY")

  return (
    <>
      <div className="page-content">
        <MetaTags>
          <title>OXYLOANS </title>
        </MetaTags>
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs
            // title={props.t("Dashboards")}
            breadcrumbItem={props.t("Dashboard")}
          />

          <Row>
            <Col xl="4">
              <WelcomeComp />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}
export default withTranslation()(Dashboard)
