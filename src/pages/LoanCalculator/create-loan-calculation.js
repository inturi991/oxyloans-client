import React, { useState } from "react"
import { useHistory } from "react-router-dom"
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Input,
  FormGroup,
  Label,
  Button,
  Alert,
} from "reactstrap"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import { AsyncTypeahead } from "react-bootstrap-typeahead"
import "react-bootstrap-typeahead/css/Typeahead.css"
import { Form, Formik } from "formik"
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import { FieldError } from "../../components/Common/FieldError"

import * as yup from "yup"

import { createSalary } from "../../services/salaries"
import { searchUserByEmail } from "../../services/users"

const schema = yup.object().shape({
  principal: yup.number().required("principal is required"),
  tenure: yup.number().required("tenure tax is required"),
  rate: yup.number().required("rate tax is required"),
})

const calculateResult = ({ principal, tenure, rate, processingFee }) => {
  const principalData = parseFloat(principal)
  const tenureData = parseFloat(tenure)
  const rateData = parseFloat(rate)
  const processingFeeData = processingFee ? parseFloat(processingFee) : 0

  const rateDataCalculation = parseFloat(rateData / 12 / 100)

  const loanEMIData =
    (principalData *
      rateDataCalculation *
      Math.pow(1 + rateDataCalculation, tenureData)) /
    (Math.pow(1 + rateDataCalculation, tenureData) - 1)

  const totalPaymentData = loanEMIData * tenureData
  const totalInterestData = totalPaymentData - principalData
  const newResults = {
    loanEMI: loanEMIData,
    totalInterestPayable: totalInterestData.toFixed(2),
    totalPayment: totalPaymentData.toFixed(2),
  }

  return newResults
}

const LoanCalculatorCreate = () => {
  const { push } = useHistory()
  const [errorMessage, setErrorMessage] = useState("")
  const [users, setUsers] = useState([])
  const [initialValues, setInitialValues] = useState({
    principal: 0,
    tenure: 0,
    rate: 0,
    processingFee: 0,
  })
  const results = calculateResult(initialValues)

  const handleCreateSalary = formValues => {
    createSalary({ ...formValues, ...results })
      .then(() => push("/salaries"))
      .catch(err => {
        if (err.response) {
          const {
            data: { message },
          } = err.response
          setErrorMessage(message)
        } else {
          setErrorMessage(err.message)
        }
      })
  }

  const handleUsersSearch = email => {
    searchUserByEmail(email).then(res => {
      setUsers(res.data)
    })
  }
  return (
    <>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Loan Calculator" breadcrumbItem="" />

          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <Row className="mb-2">
                    <Col sm="4">
                      <CardTitle className="mb-4">Loan Calculator</CardTitle>
                    </Col>
                  </Row>

                  {errorMessage && <Alert color="danger">{errorMessage}</Alert>}

                  <Formik
                    initialValues={initialValues}
                    validationSchema={schema}
                    onSubmit={handleCreateSalary}
                    validateOnChange={false}
                    validateOnBlur={false}
                  >
                    {({
                      handleChange,
                      handleSubmit,
                      values,
                      errors,
                      setFieldValue,
                    }) => {
                      return (
                        <div>
                          <Form
                            className="outer-repeater"
                            onSubmit={handleSubmit}
                          >
                            <div
                              data-repeater-list="outer-group"
                              className="outer"
                            >
                              <FormGroup className="select2-container" row>
                                <Label className="col-form-label col-lg-2">
                                  Principal (₹)
                                </Label>
                                <Col lg="10">
                                  <Row>
                                    <Col className="col-lg-6 form-group">
                                      <Input
                                        id="principal"
                                        name="principal"
                                        type="number"
                                        onChange={e => {
                                          setFieldValue(
                                            `principal`,
                                            e.target.value
                                          )
                                          setInitialValues({
                                            ...initialValues,
                                            principal: e.target.value,
                                          })
                                        }}
                                        placeholder="1000000"
                                      />
                                      <FieldError
                                        errors={errors}
                                        field="principal"
                                      />
                                    </Col>
                                  </Row>
                                </Col>
                              </FormGroup>
                              <FormGroup className="select2-container" row>
                                <Label className="col-form-label col-lg-2">
                                  Rate (%)
                                </Label>
                                <Col lg="10">
                                  <Row>
                                    <Col className="col-lg-6 form-group">
                                      <Input
                                        id="rate"
                                        name="rate"
                                        type="number"
                                        onChange={e => {
                                          setFieldValue(`rate`, e.target.value)
                                          setInitialValues({
                                            ...initialValues,
                                            rate: e.target.value,
                                          })
                                        }}
                                        placeholder="2"
                                      />
                                      <FieldError
                                        errors={errors}
                                        field="rate"
                                      />
                                    </Col>
                                  </Row>
                                </Col>
                              </FormGroup>

                              <FormGroup className="select2-container" row>
                                <Label className="col-form-label col-lg-2">
                                  Tenure (Mo)
                                </Label>
                                <Col lg="10">
                                  <Row>
                                    <Col className="col-lg-6 form-group">
                                      <Input
                                        id="tenure"
                                        name="tenure"
                                        type="number"
                                        onChange={e => {
                                          setFieldValue(
                                            `tenure`,
                                            e.target.value
                                          )
                                          setInitialValues({
                                            ...initialValues,
                                            tenure: e.target.value,
                                          })
                                        }}
                                        placeholder="12"
                                      />
                                      <FieldError
                                        errors={errors}
                                        field="tenure"
                                      />
                                    </Col>
                                  </Row>
                                </Col>
                              </FormGroup>

                              <FormGroup className="select2-container" row>
                                <Label className="col-form-label col-lg-2">
                                  Processing Fee
                                </Label>
                                <Col lg="10">
                                  <Row>
                                    <Col className="col-lg-6 form-group">
                                      <Input
                                        id="processingFee"
                                        name="processingFee"
                                        type="number"
                                        onChange={e => {
                                          setFieldValue(
                                            `processingFee`,
                                            e.target.value
                                          )
                                          setInitialValues({
                                            ...initialValues,
                                            processingFee: e.target.value,
                                          })
                                        }}
                                        placeholder="0"
                                      />
                                    </Col>
                                  </Row>
                                </Col>
                              </FormGroup>

                              <CardTitle className="mb-4"></CardTitle>

                              <div className="table-responsive">
                                <table className="table table-centered table-nowrap mb-0">
                                  <thead className="thead-light">
                                    <tr>
                                      <th>Loan EMI</th>
                                      <th>Total Interest Payable</th>
                                      <th>
                                        Total Payment(Principal + Interest)
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr key={"_tr_" + 1}>
                                      <td>{results.loanEMI}</td>
                                      <td>{results.totalInterestPayable}</td>
                                      <td>{results.totalPayment}</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </Form>
                        </div>
                      )
                    }}
                  </Formik>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

export default LoanCalculatorCreate
