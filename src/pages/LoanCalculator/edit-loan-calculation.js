import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Input,
  FormGroup,
  Label,
  Button,
  Alert,
} from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import { Form, Formik } from "formik";
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb";
import { FieldError } from "../../components/Common/FieldError";

import * as yup from "yup";

import { getSalaryById, updateSalary } from "../../services/salaries";
import { searchUserByEmail } from "../../services/users";
import { useHandleError } from "../../hooks/use-hanleerror";
const schema = yup.object().shape({
  userId: yup
    .string()

    .required("Select User"),
  salaryDate: yup.string().required("Salary Date is required"),
  ctc: yup.number().required("ctc is required"),
  incomeTax: yup.number().required("income tax is required"),
});

const calculateResult = ({
  ctc,
  incomeTax,
  professionalTax,
  employeePf,
  medicalClaim,
  parentalMedicalClaim,
}) => {
  const ctcData = parseFloat(ctc);
  const incomeTaxData = incomeTax ? parseFloat(incomeTax) : 0;
  const professionalTaxData = professionalTax ? parseFloat(professionalTax) : 0;
  const employeePfData = employeePf ? parseFloat(employeePf) : 0;
  const medicalClaimData = medicalClaim ? parseFloat(medicalClaim) : 0;
  const parentalMedicalClaimData = parentalMedicalClaim
    ? parseFloat(parentalMedicalClaim)
    : 0;

  const calculateIncomeTaxYearly = (ctcData * incomeTaxData) / 100;
  const incomeTaxMonthly = parseFloat(calculateIncomeTaxYearly / 12);
  const calculatePayYearly = ctcData;
  const grossEarning = parseFloat(calculatePayYearly / 12);
  const monthlyDeduction =
    professionalTaxData +
    incomeTaxMonthly +
    employeePfData +
    medicalClaimData +
    parentalMedicalClaimData;
  const payMonthly = grossEarning - monthlyDeduction;
  const newResults = {
    monthlyGrossEarning: grossEarning,
    yearlyGrossEarning: grossEarning * 12,
    incomeTaxMonthly: incomeTaxMonthly,
    incomeTaxYearly: calculateIncomeTaxYearly,
    monthlyDeduction: monthlyDeduction,
    payMonthly: payMonthly,
    payYearly: payMonthly * 12,
  };

  return newResults;
};

const SalaryEdit = () => {
  const { push } = useHistory();
  const { id } = useParams();
  const { errorMessage, handleError } = useHandleError();
  const [users, setUsers] = useState([]);
  const [salaryDetails, setSalaryDetails] = useState();

  useEffect(() => {
    getSalaryById(id)
      .then((res) => setSalaryDetails(res.data))
      .catch(handleError);
    //eslint-disable-next-line
  }, []);

  console.log(salaryDetails);

  const results = calculateResult({
    ctc: salaryDetails ? salaryDetails.ctc : 0,
    incomeTax: salaryDetails ? salaryDetails.incomeTax : 0,
    professionalTax: salaryDetails ? salaryDetails.professionalTax : 0,
    employeePf: salaryDetails ? salaryDetails.employeePf : 0,
    medicalClaim: salaryDetails ? salaryDetails.medicalClaim : 0,
    parentalMedicalClaim: salaryDetails
      ? salaryDetails.parentalMedicalClaim
      : 0,
  });

  const handleUpdateState = (formValues) => {
    updateSalary({ ...formValues, ...results }, id)
      .then(() => push("/salaries"))
      .catch(handleError);
  };

  const handleUsersSearch = (email) => {
    searchUserByEmail(email).then((res) => {
      setUsers(res.data);
    });
  };
  return (
    <>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Salary Calculator" breadcrumbItem="" />

          <Row>
            <Col lg="12">
              <Card>
                <CardBody>
                  <Row className="mb-2">
                    <Col sm="4">
                      <CardTitle className="mb-4">Salary</CardTitle>
                    </Col>
                    <Col sm="8">
                      <div className="text-sm-right">
                        <Button
                          type="button"
                          color="success"
                          onClick={() => push("/salaries")}
                          className="btn-rounded waves-effect waves-light mb-2 mr-2"
                        >
                          <i className="mdi mdi-keyboard-backspace mr-1"></i>
                          Back
                        </Button>
                      </div>
                    </Col>
                  </Row>

                  {errorMessage && <Alert color="danger">{errorMessage}</Alert>}
                  {salaryDetails && (
                    <Formik
                      initialValues={salaryDetails}
                      validationSchema={schema}
                      onSubmit={handleUpdateState}
                      validateOnChange={false}
                      validateOnBlur={false}
                    >
                      {({
                        handleChange,
                        handleSubmit,
                        values,
                        errors,
                        setFieldValue,
                      }) => {
                        return (
                          <div>
                            <Form
                              className="outer-repeater"
                              onSubmit={handleSubmit}
                            >
                              <div
                                data-repeater-list="outer-group"
                                className="outer"
                              >
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    User
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <AsyncTypeahead
                                          multiple={false}
                                          useCache={false}
                                          defaultInputValue={values.user.email}
                                          placeholder="User"
                                          onChange={(selected) => {
                                            if (selected.length > 0) {
                                              setFieldValue(
                                                `userId`,
                                                selected[0].id
                                              );
                                            }
                                          }}
                                          id="async-reward-types"
                                          onSearch={handleUsersSearch}
                                          labelKey="email"
                                          options={users}
                                        />
                                        <FieldError
                                          errors={errors}
                                          field="email"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Salary Date
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <DatePicker
                                          id="salaryDate"
                                          name="salaryDate"
                                          placeholderText="Date"
                                          className="form-control"
                                          selected={values.salaryDate}
                                          onChange={(date) =>
                                            setFieldValue("salaryDate", date)
                                          }
                                        />
                                        <FieldError
                                          errors={errors}
                                          field="salaryDate"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>

                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Cost to Company (CTC)
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <Input
                                          id="ctc"
                                          name="ctc"
                                          type="number"
                                          value={values.ctc}
                                          onChange={(e) => {
                                            setFieldValue(
                                              `ctc`,
                                              e.target.value
                                            );
                                            setSalaryDetails({
                                              ...salaryDetails,
                                              ctc: e.target.value,
                                            });
                                          }}
                                          placeholder="1000000"
                                        />
                                        <FieldError
                                          errors={errors}
                                          field="ctc"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>

                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Income Tax
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <Input
                                          id="incomeTax"
                                          name="incomeTax"
                                          type="number"
                                          value={values.incomeTax}
                                          onChange={(e) => {
                                            setFieldValue(
                                              `incomeTax`,
                                              e.target.value
                                            );
                                            setSalaryDetails({
                                              ...salaryDetails,
                                              incomeTax: e.target.value,
                                            });
                                          }}
                                          placeholder="12"
                                        />
                                        <FieldError
                                          errors={errors}
                                          field="incomeTax"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>

                                <CardTitle className="mb-4">
                                  Monthly Deductions
                                </CardTitle>
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Professional tax
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <Input
                                          id="professionalTax"
                                          name="professionalTax"
                                          type="number"
                                          value={values.professionalTax}
                                          onChange={(e) => {
                                            setFieldValue(
                                              `professionalTax`,
                                              e.target.value
                                            );
                                            setSalaryDetails({
                                              ...salaryDetails,
                                              professionalTax: e.target.value,
                                            });
                                          }}
                                          placeholder="200"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Employee PF Contribution
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <Input
                                          id="employeePf"
                                          name="employeePf"
                                          type="number"
                                          value={values.employeePf}
                                          onChange={(e) => {
                                            setFieldValue(
                                              `employeePf`,
                                              e.target.value
                                            );
                                            setSalaryDetails({
                                              ...salaryDetails,
                                              employeePf: e.target.value,
                                            });
                                          }}
                                          placeholder="900"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Medical Claim
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <Input
                                          id="medicalClaim"
                                          name="medicalClaim"
                                          type="number"
                                          value={values.medicalClaim}
                                          onChange={(e) => {
                                            setFieldValue(
                                              `medicalClaim`,
                                              e.target.value
                                            );
                                            setSalaryDetails({
                                              ...salaryDetails,
                                              medicalClaim: e.target.value,
                                            });
                                          }}
                                          placeholder="100"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Parental Medical Claim
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        <Input
                                          id="parentalMedicalClaim"
                                          name="parentalMedicalClaim"
                                          type="number"
                                          value={values.parentalMedicalClaim}
                                          onChange={(e) => {
                                            setFieldValue(
                                              `parentalMedicalClaim`,
                                              e.target.value
                                            );
                                            setSalaryDetails({
                                              ...salaryDetails,
                                              parentalMedicalClaim:
                                                e.target.value,
                                            });
                                          }}
                                          placeholder="100"
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>

                                <div className="table-responsive">
                                  <table className="table table-centered table-nowrap mb-0">
                                    <thead className="thead-light">
                                      <tr>
                                        <th>Particulars</th>
                                        <th>Monthly</th>
                                        <th>Yearly</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr key={"_tr_" + 1}>
                                        <td>Income Tax </td>
                                        <td>{results.incomeTaxMonthly}</td>
                                        <td>{results.incomeTaxYearly}</td>
                                      </tr>
                                      <tr key={"_tr_" + 2}>
                                        <td>Total Gross Pay </td>
                                        <td>{results.monthlyGrossEarning}</td>
                                        <td>{results.yearlyGrossEarning}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>

                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Total Monthly Deductions
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        {results.monthlyDeduction}
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>
                                <FormGroup className="select2-container" row>
                                  <Label className="col-form-label col-lg-2">
                                    Total Monthly Net Payable
                                  </Label>
                                  <Col lg="10">
                                    <Row>
                                      <Col className="col-lg-6 form-group">
                                        {results.payMonthly}
                                      </Col>
                                    </Row>
                                  </Col>
                                </FormGroup>
                              </div>
                              <Row className="justify-content-center">
                                <Col lg="2" className="md-offset-6 lg-offset-6">
                                  <Button
                                    className="btn-block"
                                    type="submit"
                                    color="primary"
                                  >
                                    Done
                                  </Button>
                                </Col>
                              </Row>
                            </Form>
                          </div>
                        );
                      }}
                    </Formik>
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default SalaryEdit;
