import React, { useEffect, useState, useRef } from "react"
import { isEmpty } from "lodash"
import moment from "moment"
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Button,
  Alert,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap"
import "react-bootstrap-typeahead/css/Typeahead.css"
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import SweetAlert from "react-bootstrap-sweetalert"
import { useHandleError } from "../../hooks/use-hanleerror"
import { useHistory } from "react-router-dom"
import { deleteSalary, getSalaries } from "../../services/salaries"
import BootstrapTable from "react-bootstrap-table-next"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import paginationFactory, {
  PaginationProvider,
  PaginationListStandalone,
} from "react-bootstrap-table2-paginator"

const LoanCalculatorPage = () => {
  const [salaries, setSalaries] = useState({
    count: 0,
    data: [],
  })

  const { push } = useHistory()
  const [pageSize, setPageSize] = useState(100)
  const [pageIndex, setPageIndex] = useState(1)
  const [sort, setSort] = useState({ sortBy: "id", sortOrder: "ASC" })
  const [searchText, setSearchText] = useState("")

  const [salaryList, setSalaryList] = useState([])
  const [deleteData, setDeleteData] = useState(null)
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false)

  const { errorMessage, handleError } = useHandleError()

  const loadSalaries = () => {
    getSalaries(pageIndex, pageSize, sort.sortBy, sort.sortOrder, searchText)
      .then(res => {
        setSalaries(res.data)
      })
      .catch(handleError)
  }
  useEffect(() => {
    loadSalaries()
    // eslint-disable-next-line
  }, [pageIndex, sort.sortBy, sort.sortOrder, searchText, pageSize])
  const onPageChange = page => {
    setPageIndex(page)
  }
  // Edit
  const handleEditClick = id => push(`/loan-calculator/${id}/edit`)
  // Add
  const handleNewClick = () => push("/loan-calculator/create")
  // Delete
  const handleDelete = id => {
    setDeleteData(id)
    setShowDeleteConfirmation(true)
    onPaginationPageChange(1)
  }
  const handleDeleteConfirm = () => {
    setShowDeleteConfirmation(false)
    deleteSalary(deleteData)
      .then(() => {
        loadSalaries()
      })
      .catch(err => {
        handleError(err)
      })
  }

  const SalariesTableColumns = [
    {
      dataField: "id",
      text: "#",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row, index) => <>{row.id}</>,
    },
    {
      dataField: "userId",
      text: "Email",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => <>{row.userId}</>,
    },
    {
      dataField: "salaryDate",
      text: "Created On",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => handleValidDate(row.salaryDate),
    },
    {
      dataField: "createdAt",
      text: "Created On",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => handleValidDate(row.createdAt),
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => (
        <>{row.status === 1 ? "Active" : "Inactive"}</>
      ),
    },
    {
      dataField: "menu",
      isDummyField: true,
      text: "Action",
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => (
        <UncontrolledDropdown direction="left">
          <DropdownToggle href="#" className="card-drop" tag="i">
            <i className="mdi mdi-dots-horizontal font-size-18" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-menu-end">
            <DropdownItem onClick={() => handleEditClick(row.id)}>
              <i className="fas fa-pencil-alt text-success me-1" />
              Edit
            </DropdownItem>
            <DropdownItem onClick={() => handleDelete(row.id)}>
              <i className="fas fa-trash-alt text-danger me-1" />
              Delete
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      ),
    },
  ]

  const handleValidDate = date => {
    const date1 = moment(new Date(date)).format("DD MMM Y")
    return date1
  }

  var node = useRef()
  const onPaginationPageChange = page => {
    if (
      node &&
      node.current &&
      node.current.props &&
      node.current.props.pagination &&
      node.current.props.pagination.options
    ) {
      node.current.props.pagination.options.onPageChange(page)
    }
  }

  const { SearchBar } = Search

  useEffect(() => {
    setSalaryList(salaries.data)
  }, [salaries.data])

  useEffect(() => {
    if (!isEmpty(salaries.data)) {
      setSalaryList(salaries.data)
    }
  }, [salaries.data])

  // eslint-disable-next-line no-unused-vars
  const handleTableChange = (type, { page, searchText }) => {
    setSalaryList(
      salaries.data.filter(customer =>
        Object.keys(customer).some(key =>
          customer[key].toLowerCase().includes(searchText.toLowerCase())
        )
      )
    )
  }

  const defaultSorted = [
    {
      dataField: "id",
      order: "asc",
    },
  ]

  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: salaries.data.length, // replace later with size(orders),
    custom: true,
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs breadcrumbItem="Loan Calculator" />

          {errorMessage && <Alert color="danger">{errorMessage}</Alert>}
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  <PaginationProvider
                    pagination={paginationFactory(pageOptions)}
                    keyField="id"
                    columns={SalariesTableColumns}
                    data={salaries.data}
                  >
                    {({ paginationProps, paginationTableProps }) => (
                      <ToolkitProvider
                        keyField="id"
                        data={salaries.data || []}
                        columns={SalariesTableColumns}
                        bootstrap4
                        search
                      >
                        {toolkitProps => (
                          <React.Fragment>
                            <Row className="mb-2">
                              <Col sm="4">
                                <div className="search-box ms-2 mb-2 d-inline-block">
                                  <div className="position-relative">
                                    <SearchBar {...toolkitProps.searchProps} />
                                    <i className="bx bx-search-alt search-icon" />
                                  </div>
                                </div>
                              </Col>
                              <Col sm="8">
                                <div className="text-sm-end">
                                  <Button
                                    type="button"
                                    color="success"
                                    className="btn-rounded  mb-2 me-2"
                                    onClick={handleNewClick}
                                  >
                                    <i className="mdi mdi-plus me-1" />
                                    Add New
                                  </Button>
                                </div>
                              </Col>
                            </Row>

                            <Row>
                              <Col xl="12">
                                <div className="table-responsive">
                                  <BootstrapTable
                                    id="table-to-xls"
                                    // id="emp"
                                    responsive
                                    bordered={false}
                                    striped={false}
                                    defaultSorted={defaultSorted}
                                    classes={"table align-middle table-nowrap"}
                                    headerWrapperClasses={"table-light"}
                                    keyField="id"
                                    {...toolkitProps.baseProps}
                                    onTableChange={handleTableChange}
                                    {...paginationTableProps}
                                    ref={node}
                                  />
                                </div>
                              </Col>
                            </Row>
                            <Row className="align-items-md-center mt-30">
                              <Col className="pagination pagination-rounded justify-content-end mb-2 inner-custom-pagination">
                                <PaginationListStandalone
                                  {...paginationProps}
                                />
                              </Col>
                            </Row>
                          </React.Fragment>
                        )}
                      </ToolkitProvider>
                    )}
                  </PaginationProvider>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      {showDeleteConfirmation && (
        <SweetAlert
          warning
          showCancel
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          onConfirm={handleDeleteConfirm}
          onCancel={() => setShowDeleteConfirmation(false)}
        >
          Are you sure you want to delete?
        </SweetAlert>
      )}
    </React.Fragment>
  )
}

export default LoanCalculatorPage
