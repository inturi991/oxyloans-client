import React from "react";
import { Row, Col, Alert, Card, CardBody, Container } from "reactstrap";

import { Link, useParams } from "react-router-dom";

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation";

// import images
import profile from "../../assets/images/profile-img.png";
import logo from "../../assets/images/logo.svg";
import { resetPassword } from "../../services/users";
import { useState } from "react";

export const ResetPasswordPage = (props) => {
  const [showPasswordMatchError, setShowPasswordMatchError] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showError, setShowError] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const { token } = useParams();
  function handleValidSubmit(event, values) {
    if (values.password !== values.confirmPassword) {
      setShowPasswordMatchError(true);
      return;
    }
    setShowPasswordMatchError(false);

    resetPassword(values, token)
      .then((res) => {
        setPassword("");
        setConfirmPassword("");
        setShowSuccess(true);
      })
      .catch((err) => setShowError(true));
  }

  return (
    <React.Fragment>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="fas fa-home h2"></i>
        </Link>
      </div>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-soft-primary">
                  <Row>
                    <Col className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">Welcome Back !</h5>
                        <p>Sign in to continue to OXYLOANS.</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end">
                      <img src={profile} alt="" className="img-fluid" />
                    </Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logo}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    {props.forgetError && props.forgetError ? (
                      <Alert color="danger" style={{ marginTop: "13px" }}>
                        {props.forgetError}
                      </Alert>
                    ) : null}
                    {props.forgetSuccessMsg ? (
                      <Alert color="success" style={{ marginTop: "13px" }}>
                        {props.forgetSuccessMsg}
                      </Alert>
                    ) : null}

                    <AvForm
                      className="form-horizontal mt-4"
                      onValidSubmit={(e, v) => handleValidSubmit(e, v)}
                    >
                      <div className="form-group">
                        <AvField
                          name="password"
                          value={password}
                          label="Password"
                          className="form-control"
                          placeholder="Enter Password"
                          type="password"
                          required
                        />
                      </div>
                      <div className="form-group">
                        <AvField
                          value={confirmPassword}
                          name="confirmPassword"
                          label="Confirm Password"
                          className="form-control"
                          placeholder="Enter Confirm Password"
                          type="password"
                          required
                        />
                      </div>

                      {showPasswordMatchError && (
                        <div className="error">Passwords should match</div>
                      )}

                      {showSuccess && (
                        <div className="success">
                          <Alert color="success">
                            Password changed.
                            <Link to="/login" className="alert-link">
                              Login
                            </Link>
                          </Alert>
                        </div>
                      )}
                      {showError && (
                        <Alert color="danger" role="alert">
                          Problem with server
                        </Alert>
                      )}
                      <Row className="form-group">
                        <Col className="text-right">
                          <button
                            className="btn btn-primary w-md waves-effect waves-light"
                            type="submit"
                          >
                            Reset
                          </button>
                        </Col>
                      </Row>
                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  Go back to{" "}
                  <Link to="login" className="font-weight-medium text-primary">
                    Login
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} GrowME.
                  <i className="mdi mdi-heart text-danger"></i>
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default ResetPasswordPage;
