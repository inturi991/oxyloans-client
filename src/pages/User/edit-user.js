import React, { useState, useEffect } from "react"
import { useHistory, useParams } from "react-router-dom"
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Input,
  Label,
  Button,
  Alert,
} from "reactstrap"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import "react-bootstrap-typeahead/css/Typeahead.css"
import { Form, Formik, ErrorMessage, Field } from "formik"
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import Select from "react-select"
import * as yup from "yup"

import { searchRoles } from "../../services/roles"

import { useHandleError } from "../../hooks/use-hanleerror"

import { searchDesignations } from "../../services/designation"

import {
  getAllGenders,
  getAllMaritalStatus,
  getAllEmployeeTypes,
} from "../../utils/seed"
import { getUserById, updateUser } from "../../services/users"
const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const schema = yup.object().shape({
  firstName: yup.string().required("First Name is required"),
  lastName: yup.string().required("Last Name is required"),
  email: yup.string().required("Email is required"),
  mobileNumber: yup
    .string()
    .required("Phone Number is required")
    .matches(phoneRegExp, "Only Numbers Allowed")
    .min(10, "Minimum 10 Digits Allowed")
    .max(10, "Maximum 10 Digits Allowed"),
  roleId: yup.string().required("Select Role"),
  designationId: yup.string().required("Select Designation"),
  gender: yup.string().required("Select Gender"),
  maritalStatus: yup.string().required("Select Marital Status"),
  employeeType: yup.string().required("Select Employee Type"),
  dateOfJoining: yup.string().required("Select  Date Of Joining"),
})

const UserEdit = () => {
  const { push } = useHistory()
  const { id } = useParams()
  const [userDetails, setUserDetails] = useState()

  const { handleError } = useHandleError()
  const [errorMessage, setErrorMessage] = useState("")
  const [roles, setRoles] = useState([])
  const [designations, setDesignations] = useState([])

  const [genders, setGenders] = useState([])

  const [maritalStatuses, setSetMaritalStatuses] = useState([])

  const [employeeTypes, setEmployeeTypes] = useState([])

  const [experiences, setExperiences] = useState([
    {
      name: "",
      title: "",
      fromDate: "",
      toDate: "",
      description: "",
    },
  ])

  useEffect(() => {
    handleGetRoleData()
    handleGetDesignationData()
    handleGetGenderData()
    handleMaritalStatusData()
    handlEemployeeTypeData()
    getUserById(id)
      .then(res => {
        setUserDetails(res.data)
        setExperiences(res.data.experiences)
      })

      .catch(handleError)
    //eslint-disable-next-line
  }, [])

  const handleGetRoleData = () => {
    searchRoles().then(res => {
      const roleData = res.data.map(role => {
        return {
          id: role.id,
          label: role.name,
          value: role.name,
        }
      })
      setRoles(roleData)
    })
  }

  const handleGetDesignationData = () => {
    searchDesignations().then(res => {
      const designationData = res.data.map(designation => {
        return {
          id: designation.id,
          label: designation.name,
          value: designation.name,
        }
      })
      setDesignations(designationData)
    })
  }

  const handleGetGenderData = () => {
    const genderData = getAllGenders.map(gender => {
      return {
        id: gender,
        label: gender,
        value: gender,
      }
    })
    setGenders(genderData)
  }

  const handleMaritalStatusData = () => {
    const maritalData = getAllMaritalStatus.map(marital => {
      return {
        id: marital,
        label: marital,
        value: marital,
      }
    })
    setSetMaritalStatuses(maritalData)
  }

  const handlEemployeeTypeData = () => {
    const employeeTypeData = getAllEmployeeTypes.map(employee => {
      return {
        id: employee,
        label: employee,
        value: employee,
      }
    })
    setEmployeeTypes(employeeTypeData)
  }

  const handleUpdateUser = formValues => {
    console.log("updated values are: ", formValues)
    updateUser({ ...formValues, experiences }, id)
      .then(() => push("/users"))
      .catch(handleError)
  }

  //https://codingstatus.com/add-and-delete-table-rows-dynamically-using-react-js/
  const handleChangeExperinces = (index, evnt) => {
    const { name, value } = evnt.target
    const rowsInput = [...experiences]
    rowsInput[index][name] = value
    setExperiences(rowsInput)
  }

  const handleChangeExperincesDate = (index, name, evnt) => {
    const rowsInput = [...experiences]
    rowsInput[index][name] = evnt
    setExperiences(rowsInput)
  }

  function handleAddRowNested() {
    const modifiedRows = [...experiences]
    modifiedRows.push({
      name: null,
      title: null,
      fromDate: null,
      toDate: null,
      description: null,
    })
    setExperiences(modifiedRows)
  }

  // function handleAddRowNested() {
  //   const modifiedRows = {
  //     name: "",
  //     title: "",
  //     fromDate: "",
  //     toDate: "",
  //     description: "",
  //   }
  //   setWorkExperinces([...workExperinces, modifiedRows])
  // }

  function handleRemoveRow(name) {
    var modifiedRows = [...experiences]
    modifiedRows = modifiedRows.filter(x => x["name"] !== name)
    setExperiences(modifiedRows)
  }

  return (
    <>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Users" breadcrumbItem="Edit User" />
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  {errorMessage && <Alert color="danger">{errorMessage}</Alert>}

                  {userDetails && (
                    <Formik
                      initialValues={userDetails}
                      validationSchema={schema}
                      onSubmit={handleUpdateUser}
                    >
                      {({
                        handleChange,
                        handleSubmit,
                        handleBlur,
                        values,
                        errors,
                        touched,
                        setFieldValue,
                      }) => {
                        return (
                          <div>
                            <Form onSubmit={handleSubmit}>
                              <Row>
                                <Col sm="6">
                                  <div className="mb-3">
                                    <Label htmlFor="firstName">
                                      First Name
                                    </Label>
                                    <Input
                                      tabIndex="1"
                                      id="firstName"
                                      name="firstName"
                                      type="text"
                                      value={values.firstName}
                                      className={
                                        "form-control" +
                                        (errors.firstName && touched.firstName
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      placeholder="Enter Your First Name"
                                    />

                                    <ErrorMessage
                                      name="firstName"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="email">Email</Label>
                                    <Input
                                      tabIndex="3"
                                      id="email"
                                      name="email"
                                      type="text"
                                      value={values.email}
                                      onChange={handleChange}
                                      className={
                                        "form-control" +
                                        (errors.email && touched.email
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                      placeholder="Enter Your Email ID"
                                    />
                                    <ErrorMessage
                                      name="email"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="mobileNumber">
                                      Mobile Number
                                    </Label>
                                    <Input
                                      tabIndex="4"
                                      id="mobileNumber"
                                      name="mobileNumber"
                                      type="text"
                                      value={values.mobileNumber}
                                      onChange={handleChange}
                                      className={
                                        "form-control" +
                                        (errors.mobileNumber &&
                                        touched.mobileNumber
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                      placeholder="Enter Your Mobile Number"
                                    />
                                    <ErrorMessage
                                      name="mobileNumber"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <label htmlFor="designationId">
                                      Designation
                                    </label>

                                    <Select
                                      defaultValue={{
                                        id: values.designationId,
                                        label: values.designationName,
                                        value: values.designationName,
                                      }}
                                      onChange={data => {
                                        setFieldValue(`designationId`, data.id)
                                      }}
                                      options={designations}
                                      name="designationId"
                                      tabIndex="1"
                                      className={
                                        "select2-selection" +
                                        (errors.designationId &&
                                        touched.designationId
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                    />
                                    <ErrorMessage
                                      name="designationId"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>

                                  <div className="mb-3">
                                    <label htmlFor="gender">Gender</label>

                                    <Select
                                      defaultValue={{
                                        id: values.gender,
                                        label: values.gender,
                                        value: values.gender,
                                      }}
                                      onChange={data => {
                                        setFieldValue(`gender`, data.id)
                                      }}
                                      options={genders}
                                      name="gender"
                                      tabIndex="1"
                                      className={
                                        "select2-selection" +
                                        (errors.gender && touched.gender
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                    />
                                    <ErrorMessage
                                      name="gender"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <label htmlFor="employeeType">
                                      Employee Type
                                    </label>

                                    <Select
                                      defaultValue={{
                                        id: values.employeeType,
                                        label: values.employeeType,
                                        value: values.employeeType,
                                      }}
                                      onChange={data => {
                                        setFieldValue(`employeeType`, data.id)
                                      }}
                                      options={employeeTypes}
                                      name="employeeType"
                                      tabIndex="1"
                                      className={
                                        "select2-selection" +
                                        (errors.employeeType &&
                                        touched.employeeType
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                    />
                                    <ErrorMessage
                                      name="employeeType"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <label htmlFor="birthDate">
                                      Birth Date
                                    </label>
                                    <DatePicker
                                      id="birthDate"
                                      name="birthDate"
                                      placeholderText="Date"
                                      className="form-control"
                                      selected={values.birthDate}
                                      onChange={date =>
                                        setFieldValue("birthDate", date)
                                      }
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="location">Location</Label>
                                    <Input
                                      tabIndex="4"
                                      id="location"
                                      name="location"
                                      type="text"
                                      value={values.location}
                                      placeholder="Enter Your Location"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                  </div>
                                </Col>
                                <Col sm="6">
                                  <div className="mb-3">
                                    <Label htmlFor="lastName">Last Name</Label>
                                    <Input
                                      tabIndex="2"
                                      id="lastName"
                                      name="lastName"
                                      type="text"
                                      value={values.lastName}
                                      className={
                                        "form-control" +
                                        (errors.lastName && touched.lastName
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      placeholder="Enter Your Last Name"
                                    />
                                    <ErrorMessage
                                      name="lastName"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="nickName">
                                      Alternative Email
                                    </Label>
                                    <Input
                                      tabIndex="3"
                                      id="alternativeEmail"
                                      name="alternativeEmail"
                                      type="text"
                                      value={values.alternativeEmail}
                                      placeholder="Enter Your Alternative Email"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="alternativeMobileNumber">
                                      Alternative Mobile Number
                                    </Label>
                                    <Input
                                      tabIndex="4"
                                      id="alternativeMobileNumber"
                                      name="alternativeMobileNumber"
                                      type="text"
                                      value={values.alternativeMobileNumber}
                                      placeholder="Enter Your Alternative Mobile Number"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <label htmlFor="roleId">Role</label>

                                    <Select
                                      defaultValue={{
                                        id: values.roleId,
                                        label: values.roleName,
                                        value: values.roleName,
                                      }}
                                      onChange={data => {
                                        setFieldValue(`roleId`, data.id)
                                      }}
                                      options={roles}
                                      name="roleId"
                                      tabIndex="1"
                                      className={
                                        "select2-selection" +
                                        (errors.roleId && touched.roleId
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                    />
                                    <ErrorMessage
                                      name="roleId"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <label htmlFor="maritalStatus">
                                      Marital Status
                                    </label>
                                    <Select
                                      defaultValue={{
                                        id: values.maritalStatus,
                                        label: values.maritalStatus,
                                        value: values.maritalStatus,
                                      }}
                                      onChange={data => {
                                        setFieldValue(`maritalStatus`, data.id)
                                      }}
                                      options={maritalStatuses}
                                      name="maritalStatus"
                                      tabIndex="1"
                                      className={
                                        "select2-selection" +
                                        (errors.maritalStatus &&
                                        touched.maritalStatus
                                          ? " is-invalid"
                                          : "")
                                      }
                                      onBlur={handleBlur}
                                    />
                                    <ErrorMessage
                                      name="maritalStatus"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="nickName">Nick Name</Label>
                                    <Input
                                      tabIndex="3"
                                      id="nickName"
                                      name="nickName"
                                      type="text"
                                      value={values.nickName}
                                      placeholder="Enter Your Nick Name"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <label htmlFor="dateOfJoining">
                                      Date Of Joining
                                    </label>
                                    <DatePicker
                                      id="dateOfJoining"
                                      name="dateOfJoining"
                                      placeholderText="Date"
                                      className="form-control"
                                      selected={values.dateOfJoining}
                                      onChange={date =>
                                        setFieldValue("dateOfJoining", date)
                                      }
                                    />
                                    <ErrorMessage
                                      name="dateOfJoining"
                                      component="div"
                                      className="invalid-feedback"
                                    />
                                  </div>
                                  <div className="mb-3">
                                    <Label htmlFor="location">
                                      Seat Location
                                    </Label>
                                    <Input
                                      tabIndex="4"
                                      id="seatLocation"
                                      name="seatLocation"
                                      type="text"
                                      value={values.seatLocation}
                                      placeholder="Enter Your  Seat Location"
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                    />
                                  </div>
                                </Col>
                              </Row>
                              <Row>
                                <Col lg={12}>
                                  <Card>
                                    <CardBody>
                                      <CardTitle className="h5 mb-4">
                                        Work Experince
                                      </CardTitle>
                                      <div className="table-responsive">
                                        {/* Render Breadcrumbs */}

                                        <table className="table table-centered table-nowrap mb-0">
                                          <thead className="thead-light">
                                            <tr>
                                              <th>Previous Company Name</th>
                                              <th>Job Title</th>
                                              <th>From Date</th>
                                              <th>From Date</th>
                                              <th>Job Description</th>
                                              <th>
                                                <Button
                                                  onClick={() => {
                                                    handleAddRowNested()
                                                  }}
                                                  color="success"
                                                  className="mt-1"
                                                >
                                                  +
                                                </Button>
                                              </th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            {(experiences || []).map(
                                              (field, idx) => (
                                                <tr key={"_tr_" + idx}>
                                                  <td>
                                                    <Input
                                                      tabIndex="3"
                                                      id={`${field.name}-${idx}`}
                                                      name="name"
                                                      type="text"
                                                      value={field.name}
                                                      onChange={evnt =>
                                                        handleChangeExperinces(
                                                          idx,
                                                          evnt
                                                        )
                                                      }
                                                      onBlur={handleBlur}
                                                    />{" "}
                                                  </td>
                                                  <td>
                                                    <Input
                                                      tabIndex="3"
                                                      id={`${field.title}-${idx}`}
                                                      name="title"
                                                      type="text"
                                                      value={field.title}
                                                      onChange={evnt =>
                                                        handleChangeExperinces(
                                                          idx,
                                                          evnt
                                                        )
                                                      }
                                                      onBlur={handleBlur}
                                                    />
                                                  </td>
                                                  <td>
                                                    {" "}
                                                    <DatePicker
                                                      id={`${field.fromDate}-${idx}`}
                                                      name="fromDate"
                                                      placeholderText="fromDate"
                                                      className="form-control"
                                                      selected={field.fromDate}
                                                      onChange={date =>
                                                        handleChangeExperincesDate(
                                                          idx,
                                                          "fromDate",
                                                          date
                                                        )
                                                      }
                                                      onBlur={handleBlur}
                                                    />
                                                  </td>
                                                  <td>
                                                    {" "}
                                                    <DatePicker
                                                      id={`${field.toDate}-${idx}`}
                                                      name="toDate"
                                                      placeholderText="toDate"
                                                      className="form-control"
                                                      selected={field.toDate}
                                                      onChange={date =>
                                                        handleChangeExperincesDate(
                                                          idx,
                                                          "toDate",
                                                          date
                                                        )
                                                      }
                                                      onBlur={handleBlur}
                                                    />
                                                  </td>
                                                  <td>
                                                    <Input
                                                      tabIndex="3"
                                                      id={`${field.description}-${idx}`}
                                                      name="description"
                                                      type="text"
                                                      value={field.description}
                                                      placeholder="Enter Your description"
                                                      onChange={evnt =>
                                                        handleChangeExperinces(
                                                          idx,
                                                          evnt
                                                        )
                                                      }
                                                      onBlur={handleBlur}
                                                    />
                                                  </td>
                                                  <td>
                                                    {" "}
                                                    <Button
                                                      color="primary"
                                                      className="btn-block inner"
                                                      id="unknown-btn"
                                                      onClick={e => {
                                                        handleRemoveRow(
                                                          field.name
                                                        )
                                                      }}
                                                    >
                                                      -
                                                    </Button>
                                                  </td>
                                                </tr>
                                              )
                                            )}
                                          </tbody>
                                        </table>
                                      </div>
                                    </CardBody>
                                  </Card>
                                </Col>
                              </Row>
                              <div className="d-flex flex-wrap gap-2">
                                <Button
                                  type="submit"
                                  color="primary"
                                  className="btn "
                                >
                                  Save
                                </Button>
                                <Button
                                  type="button"
                                  color="secondary"
                                  className=" "
                                  onClick={() => push("/users")}
                                >
                                  Cancel
                                </Button>
                              </div>
                            </Form>
                          </div>
                        )
                      }}
                    </Formik>
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

export default UserEdit
