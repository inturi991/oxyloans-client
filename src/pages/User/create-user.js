import React, { useState, useEffect } from "react"
import { useHistory } from "react-router-dom"
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Input,
  Label,
  Button,
  Alert,
} from "reactstrap"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "react-bootstrap-typeahead/css/Typeahead.css"
import { Form, Formik, ErrorMessage, Field } from "formik"
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import Select from "react-select"
import * as yup from "yup"

import { signUp } from "../../services/users"
import { searchRoles } from "../../services/roles"

import { searchDesignations } from "../../services/designation"

import {getAllGenders,getAllMaritalStatus,getAllEmployeeTypes} from "../../utils/seed"

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const schema = yup.object().shape({
  firstName: yup.string().required("First Name is required"),
  lastName: yup.string().required("Last Name is required"),
  email: yup.string().required("Email is required"),
  mobileNumber: yup
    .string()
    .required("Phone Number is required")
    .matches(phoneRegExp, "Only Numbers Allowed")
    .min(10, "Minimum 10 Digits Allowed")
    .max(10, "Maximum 10 Digits Allowed"),
  roleId: yup.string().required("Select Role"),
  designationId: yup.string().required("Select Designation"),
  gender: yup.string().required("Select Gender"),
  maritalStatus: yup.string().required("Select Marital Status"),
  employeeType: yup.string().required("Select Employee Type"),
  dateOfJoining: yup.string().required("Select  Date Of Joining"),
  password: yup.string().required("Password is required"),
  confirmPassword: yup.string().required("Confirm Password is required"),
})


const UserCreate = () => {
  const { push } = useHistory()
  const [errorMessage, setErrorMessage] = useState("")


  const [roles, setRoles] = useState([])
  const [designations, setDesignations] = useState([])

  const [genders, setGenders] = useState([])

  const [maritalStatuses, setSetMaritalStatuses] = useState([])

  const [employeeTypes, setEmployeeTypes] = useState([])

  const initialValues = {
    firstName: "",
    lastName: "",
    email: "",
    alternativeEmail:"",
    nickName: "",
    roleId: "",
    designationId:"",
    mobileNumber:"",
    alternativeMobileNumber: "",
    gender: "",
    maritalStatus:"",
    employeeType:"",
    birthDate:"",
    dateOfJoining:"",
    password: "",
    confirmPassword: "",
  }



  useEffect(() => {
    handleGetRoleData()
    handleGetDesignationData()
    handleGetGenderData()
    handleMaritalStatusData()
    handlEemployeeTypeData()
  }, [])

  const handleGetRoleData = () => {
    searchRoles().then(res => {
      const roleData = res.data.map(role => {
        return {
          id: role.id,
          label: role.name,
          value: role.name,
        }
      })
      setRoles(roleData)
    })
  }

  const handleGetDesignationData = () => {
    searchDesignations().then(res => {
      const designationData = res.data.map(designation => {
        return {
          id: designation.id,
          label: designation.name,
          value: designation.name,
        }
      })
      setDesignations(designationData)
    })
  }

  const handleGetGenderData = () => {
      const genderData = getAllGenders.map(gender => {
        return {
          id: gender,
          label: gender,
          value: gender,
        }
      })
      setGenders(genderData)
    }
  

  const handleMaritalStatusData = () => {
    const maritalData = getAllMaritalStatus.map(marital => {
      return {
        id: marital,
        label: marital,
        value: marital,
      }
    })
    setSetMaritalStatuses(maritalData)
  }

  const handlEemployeeTypeData = () => {
    const employeeTypeData = getAllEmployeeTypes.map(employee => {
      return {
        id: employee,
        label: employee,
        value: employee,
      }
    })
    setEmployeeTypes(employeeTypeData)
  }

  const handleCreateUser = formValues => {
    signUp({ ...formValues })
      .then(() => push("/users"))
      .catch(err => {
        if (err.response) {
          const {
            data: { message },
          } = err.response
          setErrorMessage(message)
        } else {
          setErrorMessage(err.message)
        }
      })
  }


  return (
    <>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumbs */}
          <Breadcrumbs title="Users" breadcrumbItem="Create User" />

          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  {errorMessage && <Alert color="danger">{errorMessage}</Alert>}
                  <Formik
                    initialValues={initialValues}
                    validationSchema={schema}
                    onSubmit={handleCreateUser}
                    // validateOnChange={false}
                    // validateOnBlur={false}
                  >
                    {({
                      handleChange,
                      handleSubmit,
                      handleBlur,
                      values,
                      errors,
                      touched,
                      setFieldValue,
                    }) => {
                      return (
                        <div>
                          <Form onSubmit={handleSubmit}>
                            <Row>
                              <Col sm="6">
                                <div className="mb-3">
                                  <Label htmlFor="firstName">First Name</Label>
                                  <Field
                                    tabIndex="1"
                                    id="firstName"
                                    name="firstName"
                                    className={
                                      "form-control" +
                                      (errors.firstName && touched.firstName
                                        ? " is-invalid"
                                        : "")
                                    }
                                    type="text"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    placeholder="Enter Your First Name"
                                  />

                                  <ErrorMessage
                                    name="firstName"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="email">Email</Label>
                                  <Input
                                    tabIndex="3"
                                    id="email"
                                    name="email"
                                    type="text"
                                    className={
                                      "form-control" +
                                      (errors.email && touched.email
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Email ID"
                                  />

                                  <ErrorMessage
                                    name="email"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="mobileNumber">Mobile Number</Label>
                                  <Input
                                    tabIndex="4"
                                    id="mobileNumber"
                                    name="mobileNumber"
                                    type="text"
                                    className={
                                      "form-control" +
                                      (errors.mobileNumber && touched.mobileNumber
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Mobile Number"
                                  />
                                  <ErrorMessage
                                    name="mobileNumber"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <label htmlFor="designationId">Designation</label>

                                  <Select
                                    onChange={data => {
                                      setFieldValue(`designationId`, data.id)
                                    }}
                                    options={designations}
                                    name="designationId"
                                    tabIndex="1"
                                    className={"select2-selection" +
                                    (errors.designationId && touched.designationId
                                      ? " is-invalid"
                                      : "")}
                                    onBlur={handleBlur}
                                  />
                                  <ErrorMessage
                                    name="designationId"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <label htmlFor="gender">Gender</label>

                                  <Select
                                    onChange={data => {
                                      setFieldValue(`gender`, data.id)
                                    }}
                                    options={genders}
                                    name="gender"
                                    tabIndex="1"
                                    className={"select2-selection" +
                                    (errors.gender && touched.gender
                                      ? " is-invalid"
                                      : "")}
                                    onBlur={handleBlur}
                                  />
                                   <ErrorMessage
                                    name="gender"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <label htmlFor="employeeType">Employee Type</label>

                                  <Select
                                    onChange={data => {
                                      setFieldValue(`employeeType`, data.id)
                                    }}
                                    options={employeeTypes}
                                    name="employeeType"
                                    tabIndex="1"
                                    className={"select2-selection" +
                                    (errors.employeeType && touched.employeeType
                                      ? " is-invalid"
                                      : "")}
                                    onBlur={handleBlur}
                                  />
                                   <ErrorMessage
                                    name="employeeType"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                <label htmlFor="birthDate">Birth Date</label>
                                      <DatePicker
                                        id="birthDate"
                                        name="birthDate"
                                        placeholderText="Date"
                                        className="form-control"
                                        selected={values.birthDate}
                                        onChange={(date) =>
                                          setFieldValue("birthDate", date)
                                        }
                                      />
                              </div>
                                <div className="mb-3">
                                  <Label htmlFor="password">Password</Label>
                                  <Input
                                    tabIndex="9"
                                    id="password"
                                    name="password"
                                    type="password"
                                    placeholder="Enter Your Password"
                                    onChange={handleChange}
                                    className={
                                      "form-control" +
                                      (errors.password && touched.password
                                        ? " is-invalid"
                                        : "")
                                    }
                                  />
                                  <ErrorMessage
                                    name="password"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                              </Col>
                              <Col sm="6">
                                <div className="mb-3">
                                  <Label htmlFor="lastName">Last Name</Label>
                                  <Input
                                    tabIndex="2"
                                    id="lastName"
                                    name="lastName"
                                    type="text"
                                    onChange={handleChange}
                                    className={
                                      "form-control" +
                                      (errors.lastName && touched.lastName
                                        ? " is-invalid"
                                        : "")
                                    }
                                    onBlur={handleBlur}
                                    placeholder="Enter Your Last Name"
                                  />
                                  <ErrorMessage
                                    name="lastName"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="nickName">Alternative Email</Label>
                                  <Input
                                    tabIndex="3"
                                    id="alternativeEmail"
                                    name="alternativeEmail"
                                    type="text"
                                    placeholder="Enter Your Alternative Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="alternativeMobileNumber">Alternative Mobile Number</Label>
                                  <Input
                                    tabIndex="4"
                                    id="alternativeMobileNumber"
                                    name="alternativeMobileNumber"
                                    type="text"
                                    placeholder="Enter Your Alternative Mobile Number"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                 
                                </div>
                                <div className="mb-3">
                                  <label htmlFor="roleId">Role</label>

                                  <Select
                                    onChange={data => {
                                      setFieldValue(`roleId`, data.id)
                                    }}
                                    options={roles}
                                    name="roleId"
                                    tabIndex="1"
                                    className={"select2-selection" +
                                    (errors.roleId && touched.roleId
                                      ? " is-invalid"
                                      : "")}
                                    onBlur={handleBlur}
                                  />
                                   <ErrorMessage
                                    name="roleId"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <label htmlFor="maritalStatus">Marital Status</label>
                                  <Select
                                    onChange={data => {
                                      setFieldValue(`maritalStatus`, data.id)
                                    }}
                                    options={maritalStatuses}
                                    name="maritalStatus"
                                    tabIndex="1"
                                    className={"select2-selection" +
                                    (errors.maritalStatus && touched.maritalStatus
                                      ? " is-invalid"
                                      : "")}
                                    onBlur={handleBlur}
                                  />
                                   <ErrorMessage
                                    name="maritalStatus"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                                <div className="mb-3">
                                  <Label htmlFor="nickName">Nick Name</Label>
                                  <Input
                                    tabIndex="3"
                                    id="nickName"
                                    name="nickName"
                                    type="text"
                                    placeholder="Enter Your Nick Name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                  />
                                </div>
                                <div className="mb-3">
                                <label htmlFor="dateOfJoining">Date Of Joining</label>
                                      <DatePicker
                                        id="dateOfJoining"
                                        name="dateOfJoining"
                                        placeholderText="Date"
                                        className="form-control"
                                        selected={values.dateOfJoining}
                                        onChange={(date) =>
                                          setFieldValue("dateOfJoining", date)
                                        }
                                      />
                                  <ErrorMessage
                                    name="dateOfJoining"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                              </div>
                                <div className="mb-3">
                                  <Label htmlFor="confirmPassword">
                                    Confirm Password
                                  </Label>
                                  <Input
                                    tabIndex="10"
                                    id="confirmPassword"
                                    name="confirmPassword"
                                    type="password"
                                    placeholder="Enter Your Confirm Password"
                                    onChange={handleChange}
                                    className={
                                      "form-control" +
                                      (errors.confirmPassword &&
                                      touched.confirmPassword
                                        ? " is-invalid"
                                        : "")
                                    }
                                  />

                                  <ErrorMessage
                                    name="confirmPassword"
                                    component="div"
                                    className="invalid-feedback"
                                  />
                                </div>
                              </Col>
                            </Row>
                            <div className="d-flex flex-wrap gap-2">
                              <Button
                                type="submit"
                                color="primary"
                                className="btn "
                              >
                                Save
                              </Button>
                              <Button
                                type="button"
                                color="secondary"
                                className=" "
                                onClick={() => push("/users")}
                              >
                                Cancel
                              </Button>
                            </div>
                          </Form>
                        </div>
                      )
                    }}
                  </Formik>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

export default UserCreate
