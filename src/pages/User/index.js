import React, { useEffect, useState, useRef } from "react"
import * as moment from "moment"
import { isEmpty } from "lodash"
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Row,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
  Alert,
} from "reactstrap"
import SweetAlert from "react-bootstrap-sweetalert"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"
import { useHandleError } from "../../hooks/use-hanleerror"
import { useHistory } from "react-router-dom"
import { deleteUser, getAllUsers } from "../../services/users"
import ReactExport from "react-export-excel"

import images from "assets/images"

const UserPage = props => {
  const [modal, setModal] = useState(false)
  const [customerList, setCustomerList] = useState([])
  const [isEdit, setIsEdit] = useState(false)
  const [pageSize, setPageSize] = useState(100)
  const [pageIndex, setPageIndex] = useState(1)
  const [sort, setSort] = useState({ sortBy: "id", sortOrder: "ASC" })
  const [searchText, setSearchText] = useState("")
  const { push } = useHistory()
  const [users, setUsers] = useState({
    count: 0,
    data: [],
  })

  const { errorMessage, handleError } = useHandleError()
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false)
  const [deleteData, setDeleteData] = useState(null)

  const loadTeams = () => {
    getAllUsers(pageIndex, pageSize, sort.sortBy, sort.sortOrder, searchText)
      .then(res => {
        setUsers(res.data)
      })
      .catch(handleError)
  }
  useEffect(() => {
    loadTeams()
  }, [pageIndex, sort.sortBy, sort.sortOrder, searchText, pageSize])

  const handleNewClick = () => push("/users/create")
  // Edit
  const handleEditClick = id => push(`/users/${id}/edit`)
  // Delete
  const handleDelete = id => {
    setDeleteData(id)
    setShowDeleteConfirmation(true)
    onPaginationPageChange(1)
  }
  const handleDeleteConfirm = () => {
    setShowDeleteConfirmation(false)
    deleteUser(deleteData)
      .then(() => {
        loadTeams()
      })
      .catch(err => {
        handleError(err)
      })
  }

  const userStatus = [
    {
      value: "0",
      label: "Delete",
    },
    {
      value: "1",
      label: "Active",
    },
    {
      value: "2",
      label: "In Active",
    },
  ]
  const getStatusLabel = value => {
    console.log("GET VALUE STATUS", value, typeof value)
    // console.log(
    //   "GET STATUS LABEL",
    //   userCandidateStatus.find(status => status.value == value).label || ""
    // )

    return userStatus.find(status => status.value == value).label || ""
  }

  const Status = () => {
    return userStatus.map(list => {
      return (
        <option key={list.label} value={list.value}>
          {list.label}
        </option>
      )
    })
  }

  const handleValidDate = date => {
    const date1 = moment(new Date(date)).format("DD MMM Y")
    return date1
  }

  //pagination customization
  const pageOptions = () => {
    return {
      sizePerPage: 10,
      totalSize: users.data.length, // replace later with size(orders),
      custom: true,
    }
  }

  const UserListColumns = [
    {
      dataField: "id",
      text: "#",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row, index) => <>{row.id}</>,
    },
    {
      dataField: "profileImage",
      text: "Photo",
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => (
        <>
          {!row.profileImageUrl ? (
            <div className="avatar-xs">
              <span className="avatar-title rounded-circle">
                {row.firstName.charAt(0)}
              </span>
            </div>
          ) : (
            <div>
              <img
                className="rounded-circle avatar-xs"
                src={row.profileImageUrl}
                alt=""
              />
            </div>
          )}
        </>
      ),
    },
    {
      dataField: "employeeId",
      text: "Employee Id",
      sort: true,
    },
    {
      dataField: "firstName",
      text: "First Name",
      sort: true,
    },
    {
      dataField: "lastName",
      text: "Last Name",
      sort: true,
    },
    {
      dataField: "email",
      text: "Email",
      sort: true,
    },

    {
      dataField: "gender",
      text: "Gender",
      sort: true,
    },
    {
      dataField: "qualification",
      text: "Qualification",
      sort: true,
    },
    {
      dataField: "roleName",
      text: "Role",
      sort: true,
    },
    {
      dataField: "mobile",
      text: "Mobile",
      sort: true,
    },

    {
      dataField: "createdAt",
      text: "Created On",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => handleValidDate(row.createdAt),
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => <>{getStatusLabel(row.status)}</>,
    },
    {
      dataField: "menu",
      isDummyField: true,
      text: "Action",
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => (
        <UncontrolledDropdown direction="left">
          <DropdownToggle href="#" className="card-drop" tag="i">
            <i className="mdi mdi-dots-horizontal font-size-18" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-menu-end">
            <DropdownItem onClick={() => handleEditClick(row.id)}>
              <i className="fas fa-pencil-alt text-success me-1" />
              Edit
            </DropdownItem>
            <DropdownItem onClick={() => handleDelete(row.id)}>
              <i className="fas fa-trash-alt text-danger me-1" />
              Delete
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      ),
    },
  ]

  var node = useRef()
  const onPaginationPageChange = page => {
    if (
      node &&
      node.current &&
      node.current.props &&
      node.current.props.pagination &&
      node.current.props.pagination.options
    ) {
      node.current.props.pagination.options.onPageChange(page)
    }
  }

  const { SearchBar } = Search

  useEffect(() => {
    setCustomerList(users.data)
  }, [users.data])

  useEffect(() => {
    if (!isEmpty(users.data)) {
      setCustomerList(users.data)
    }
  }, [users.data])

  // eslint-disable-next-line no-unused-vars
  const handleTableChange = (type, { page, searchText }) => {
    setCustomerList(
      users.data.filter(customer =>
        Object.keys(customer).some(key =>
          customer[key].toLowerCase().includes(searchText.toLowerCase())
        )
      )
    )
  }

  const defaultSorted = [
    {
      dataField: "id",
      order: "asc",
    },
  ]

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs breadcrumbItem="Users" />

          {errorMessage && <Alert color="danger">{errorMessage}</Alert>}
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  <PaginationProvider
                    pagination={paginationFactory(pageOptions())}
                    keyField="id"
                    columns={UserListColumns}
                    data={users.data}
                  >
                    {({ paginationProps, paginationTableProps }) => (
                      <ToolkitProvider
                        keyField="id"
                        data={users.data || []}
                        columns={UserListColumns}
                        bootstrap4
                        search
                      >
                        {toolkitProps => (
                          <React.Fragment>
                            <Row className="mb-2">
                              <Col sm="4">
                                <div className="search-box ms-2 mb-2 d-inline-block">
                                  <div className="position-relative">
                                    <SearchBar {...toolkitProps.searchProps} />
                                    <i className="bx bx-search-alt search-icon" />
                                  </div>
                                </div>
                              </Col>
                              <Col sm="8">
                                <div className="text-sm-end">
                                  <Button
                                    type="button"
                                    color="success"
                                    className="btn-rounded  mb-2 me-2"
                                    onClick={handleNewClick}
                                  >
                                    <i className="mdi mdi-plus me-1" />
                                    Add New User
                                  </Button>
                                </div>
                              </Col>
                            </Row>

                            <Row>
                              <Col xl="12">
                                <div className="table-responsive">
                                  <BootstrapTable
                                    id="table-to-xls"
                                    // id="emp"
                                    responsive
                                    bordered={false}
                                    striped={false}
                                    defaultSorted={defaultSorted}
                                    classes={"table align-middle table-nowrap"}
                                    headerWrapperClasses={"table-light"}
                                    keyField="id"
                                    {...toolkitProps.baseProps}
                                    onTableChange={handleTableChange}
                                    {...paginationTableProps}
                                    ref={node}
                                  />
                                </div>
                              </Col>
                            </Row>
                            <Row className="align-items-md-center mt-30">
                              <Col className="pagination pagination-rounded justify-content-end mb-2 inner-custom-pagination">
                                <PaginationListStandalone
                                  {...paginationProps}
                                />
                              </Col>
                            </Row>
                          </React.Fragment>
                        )}
                      </ToolkitProvider>
                    )}
                  </PaginationProvider>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      {showDeleteConfirmation && (
        <SweetAlert
          warning
          showCancel
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          onConfirm={handleDeleteConfirm}
          onCancel={() => setShowDeleteConfirmation(false)}
        >
          Are you sure you want to delete?
        </SweetAlert>
      )}
    </React.Fragment>
  )
}

export default UserPage
