import React from "react"
import { Redirect } from "react-router-dom"

// Profile
import UserProfile from "../pages/Authentication/user-profile"

// // Pages Component
import Chat from "../pages/Chat/Chat"

// Authentication related pages
import Login from "../pages/Authentication/Login"
import Logout from "../pages/Authentication/Logout"
import Register from "../pages/Authentication/Register"
import ForgetPwd from "../pages/Authentication/ForgetPassword"
import ResetPasswordPage from "../pages/Authentication/ResetPassword"
import Users from "../pages/User"
import UserCreate from "../pages/User/create-user"
import UserEdit from "../pages/User/edit-user"

import LoanCalculator from "../pages/LoanCalculator"
import LoanCalculatorCreate from "../pages/LoanCalculator/create-loan-calculation"
import SalaryEdit from "../pages/LoanCalculator/edit-loan-calculation"

// Dashboard
import Dashboard from "../pages/Dashboard/index"

const userRoutes = [
  {
    path: "/dashboard",
    component: Dashboard,
    role: "SuperAdmin",
  },

  { path: "/chat", component: Chat, role: "SuperAdmin" },

  // //profile
  {
    path: "/profile",
    component: UserProfile,
    role: "SuperAdmin",
  },

  { path: "/users", component: Users, role: "SuperAdmin", exact: true },

  {
    path: "/users/create",
    component: UserCreate,
    role: "SuperAdmin",
  },
  {
    path: "/users/:id/edit",
    component: UserEdit,
    role: "SuperAdmin",
  },

  {
    path: "/loan-calculator",
    component: LoanCalculator,
    role: "SuperAdmin",
    exact: true,
  },
  {
    path: "/loan-calculator/create",
    component: LoanCalculatorCreate,
    role: "SuperAdmin",
  },
  {
    path: "/loan-calculator/:id/edit",
    component: SalaryEdit,
    role: "SuperAdmin",
  },

  // eslint-disable-next-line react/display-name
  // this route should be at the end of all other routes
  { path: "/", exact: true, component: () => <Redirect to="/login" /> },
]

const authRoutes = [
  { path: "/logout", component: Logout },
  { path: "/login", component: Login },
  { path: "/forgot-password", component: ForgetPwd },
  { path: "/register", component: Register },
  {
    path: "/api/user/change_password/:token",
    component: ResetPasswordPage,
    exact: true,
  },
]

export { userRoutes, authRoutes }
