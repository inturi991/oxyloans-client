import React from "react"
import { Route, Redirect, withRouter } from "react-router-dom"
import Login from "pages/Authentication/Login";
//import PageNotFound from "./PageNotFound"

const Authmiddleware = ({
  component: Component,
  layout: Layout,
  exact,
  path,
  role,
  roleDataEntry,
  roleCallers,
  roleOutBoundCallers,
}) => (
  <Route
    path={path}
    exact={exact}
    render={props => {
      // here you can apply condition
      if (
        !localStorage.getItem("authUser") &&
        !props.location.pathname.includes("change_password")
      ) {
        return (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }

      return (
        <>
          {JSON.parse(localStorage.getItem("authUser"))?.user?.roleName ==
            role ||
          JSON.parse(localStorage.getItem("authUser"))?.user?.roleName ==
            roleDataEntry ||
          JSON.parse(localStorage.getItem("authUser"))?.user?.roleName ==
            roleCallers ||
          JSON.parse(localStorage.getItem("authUser"))?.user?.roleName ==
            roleOutBoundCallers ? (
            <Layout>
              <Component {...props} />
            </Layout>
          ) : (
             <Login />
            // localStorage.removeItem("authUser"),
            // localStorage.removeItem("role"),
            // localStorage.removeItem("I18N_LANGUAGE"),
            // localStorage.removeItem("i18nextLng")
          )}
        </>
      )
    }}
  />
)

export default withRouter(Authmiddleware)
