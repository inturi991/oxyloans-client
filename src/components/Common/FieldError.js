import React from "react";
import { Alert } from "reactstrap";
export const FieldError = ({ errors, field }) => {
  return errors[field] ? <Alert color="danger">{errors[field]}</Alert> : null;
};

export const SimpleFieldError = ({ error }) => {
  return error ? <div className="invalid-feedback">{error}</div> : null;
};
