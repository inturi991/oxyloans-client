import React, { useState } from "react";
import SweetAlert from "react-bootstrap-sweetalert";

export const DeleteButton = ({ rowKey, id, handleDeleteConfirm }) => {
  //console.log("DELETEBUTTON COMMON SERVICE",rowKey,id)
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const handleDeleteClick = (id) => {
    if (rowKey === id) {
      setShowDeleteConfirmation(true);
    }
  };
  return (
    <>
      <button
        onClick={() => handleDeleteClick(id)}
        type="button"
        className="btn btn-danger waves-effect waves-light"
      >
        <i className="bx bx-trash font-size-16 align-middle"></i>
      </button>
      {showDeleteConfirmation && rowKey === id && (
        <SweetAlert
          warning
          showCancel
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          onConfirm={() => {
            setShowDeleteConfirmation(false);
            handleDeleteConfirm(id);
          }}
          onCancel={() => setShowDeleteConfirmation(false)}
        >
          Are you sure you want to delete?
        </SweetAlert>
      )}
    </>
  );
};
