import Axios from "../helpers/axios-config";

export const getDesignations = (
  pageIndex,
  pageSize,
  sortBy,
  sortOrder,
  searchText
) => {
  return Axios.get(
    `api/designations?pageIndex=${pageIndex}&pageSize=${pageSize}&propertyName=${sortBy}&sortOrder=${sortOrder}&search=${searchText}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  );
};

export const createDesignation = (payload) => {
  return Axios.post(`api/designations`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const updateDesignation = (payload) => {
  return Axios.put(`api/designations/${payload.id}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const getDesignationById = (id) => {
  return Axios.get(`api/designations/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const deleteDesignation = (id) => {
  return Axios.delete(`api/designations/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const searchDesignations = () => {
  return Axios.get(`api/designations/designation-name`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

