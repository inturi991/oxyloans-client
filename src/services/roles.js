import Axios from "../helpers/axios-config";

export const getRoles = (
  pageIndex,
  pageSize,
  sortBy,
  sortOrder,
  searchText
) => {
  return Axios.get(
    `api/roles?pageIndex=${pageIndex}&pageSize=${pageSize}&propertyName=${sortBy}&sortOrder=${sortOrder}&search=${searchText}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  );
};

export const createRole = (payload) => {
  return Axios.post(`api/roles`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const updateRole = (payload) => {
  return Axios.put(`api/roles/${payload.id}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const getRoleById = (id) => {
  return Axios.get(`api/roles/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const deleteRole = (id) => {
  return Axios.delete(`api/roles/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const searchRoles = () => {
  return Axios.get(`api/roles/role-name`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const getRolesDropDown = () => {
  return Axios.get(`api/roles`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};
