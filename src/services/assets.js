import Axios from "../helpers/axios-config"

export const getAllAssets = (
  pageIndex,
  pageSize,
  sortBy,
  sortOrder,
  searchText
) => {
  return Axios.get(
    `api/assets?pageIndex=${pageIndex}&pageSize=${pageSize}&propertyName=${sortBy}&sortOrder=${sortOrder}&search=${searchText}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  )
}

export const createAsset = payload => {
  return Axios.post(`api/assets`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const updateAsset = payload => {
  return Axios.put(`api/assets/${payload.id}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const getAssetById = id => {
  return Axios.get(`api/assets/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const deleteAsset = id => {
  return Axios.delete(`api/assets/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const searchassets = name => {
  return Axios.get(`api/assets/Asset-name?name=${name}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const getassetsDropDown = () => {
  return Axios.get(`api/assets`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}
