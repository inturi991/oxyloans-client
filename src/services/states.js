import Axios from "../helpers/axios-config"

export const getAllStates = (
  pageIndex,
  pageSize,
  sortBy,
  sortOrder,
  searchText
) => {
  return Axios.get(
    `api/states?pageIndex=${pageIndex}&pageSize=${pageSize}&propertyName=${sortBy}&sortOrder=${sortOrder}&search=${searchText}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  )
}

export const createState = payload => {
  return Axios.post(`api/states`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const updateState = payload => {
  return Axios.put(`api/states/${payload.id}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const getStateById = id => {
  return Axios.get(`api/states/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const deleteState = id => {
  return Axios.delete(`api/states/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const searchStates = name => {
  return Axios.get(`api/states/state-name?name=${name}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const getStatesDropDown = () => {
  return Axios.get(`api/states`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}
