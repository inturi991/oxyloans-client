import Axios from "../helpers/axios-config"

export const getAllUsers = (
  pageIndex,
  pageSize,
  sortBy,
  sortOrder,
  searchText
) => {
  return Axios.get(
    `api/users?pageIndex=${pageIndex}&pageSize=${pageSize}&propertyName=${sortBy}&orderBy=${sortOrder}&search=${searchText}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  )
}

export const signUp = payload => {
  return Axios.post(`api/auth/sign-up`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const getUserById = userId => {
  return Axios.get(`api/users/${userId}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const updateUser = payload => {
  return Axios.put(`api/users/${payload.id}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const searchUser = name => {
  return Axios.get(`api/users/user-name?name=${name}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const searchUserByEmail = name => {
  return Axios.get(`api/users/user-email?email=${name}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const changePassword = payload => {
  return Axios.post(`api/users/change_password`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const resetPassword = (payload, token) => {
  return Axios.post(`api/users/change_password/${token}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const deleteUser = id => {
  console.log("DELETE API ID", `api/users/${id}`, id)
  return Axios.delete(`api/users/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}
