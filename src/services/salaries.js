import Axios from "../helpers/axios-config";

export const getSalaries = (
  pageIndex,
  pageSize,
  sortBy,
  sortOrder,
  searchText
) => {
  return Axios.get(
    `api/salaries?pageIndex=${pageIndex}&pageSize=${pageSize}&propertyName=${sortBy}&sortOrder=${sortOrder}&search=${searchText}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  );
};

export const createSalary = (payload) => {
  return Axios.post(`api/salaries`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const updateSalary = (payload) => {
  return Axios.put(`api/salaries/${payload.id}`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const getSalaryById = (id) => {
  return Axios.get(`api/salaries/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};

export const deleteSalary = (id) => {
  return Axios.delete(`api/salaries/${id}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  });
};
