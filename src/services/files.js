import Axios from "../helpers/axios-config"

export const uploadFileInFolder = payload => {
  return Axios.post(`api/files`, payload, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
    },
  })
}

export const downloadFileInFolder = (bucketName, fileName) => {
  return Axios.get(`/api/files?bucketName=${bucketName}&fileName=${fileName}`, {
    headers: {
      Authorization: `Bearer ${
        JSON.parse(localStorage.getItem("authUser")).token
      }`,
      "Content-Type": "application/octet-stream",
      "Content-Disposition": "attachment; filename=" + fileName,
    },
  })
}
export const deleteFileInFolder = (bucketName, fileName) => {
  return Axios.delete(
    `/api/files?bucketName=${bucketName}&fileName=${fileName}`,
    {
      headers: {
        Authorization: `Bearer ${
          JSON.parse(localStorage.getItem("authUser")).token
        }`,
      },
    }
  )
}
