import { useState } from "react";

export const useHandleError = () => {
  const [errorMessage, setErrorMessage] = useState("");
  const handleError = (err) => {
    if (err.response) {
      const {
        data: { message },
      } = err.response;
      setErrorMessage(message);
    } else {
      setErrorMessage(err.message);
    }
  };
  return { handleError, errorMessage };
};
