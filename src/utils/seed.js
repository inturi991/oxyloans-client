export const getAllGenders = ["Male", "Female"]

export const getAllMaritalStatus = ["Single", "Married", "Other"]

export const getAllEmployeeTypes = [
  "Permanent",
  "FullTime",
  "PartTime",
  "Trainee",
  "Interns",
  "Other",
]

export const getAllAssetTypes = [
  "Laptop",
  "Mouse",
  "KeyBoard",
  "HeadSet",
  "Monitor",
  "CPU",
  "Phone",
  "Ipad",
  "Other",
]
